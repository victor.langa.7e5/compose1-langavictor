/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf(
            Affirmation(11232312, R.string.affirmation1, R.drawable.image1, R.string.description1, "666666666", "mondongo@gmail.com", "merequetenge@gmail.com"),
            Affirmation(23413245, R.string.affirmation2, R.drawable.image2, R.string.description2, "777777777", "tomaMango@gmail.com", "victorLeDiceAJoel@gmail.com"),
            Affirmation(35468212, R.string.affirmation3, R.drawable.image3, R.string.description3, "888888888", "desayunaConHuevo@gmail.com", "quisieraSerUnaMosca@gmail.com"),
            Affirmation(96873515, R.string.affirmation4, R.drawable.image4, R.string.description4, "999999999", "eldenRingSkrrr@gmail.com", "viernesssssss@gmail.com"),
            Affirmation(68751423, R.string.affirmation5, R.drawable.image5, R.string.description5, "000000000", "unLagartinho@gmail.com", "estaMortoooo@gmail.com"),
            Affirmation(21254354, R.string.affirmation6, R.drawable.image6, R.string.description6, "111111111", "topGatosRefacheros@gmail.com", "top5GatoBlutuch@gmail.com"),
            Affirmation(68578565, R.string.affirmation7, R.drawable.image7, R.string.description7, "222222222", "link@gmail.com", "zelda@gmail.com"),
            Affirmation(35735414, R.string.affirmation8, R.drawable.image8, R.string.description8, "333333333", "silksong@gmail.com", "tititiririritiritiri@gmail.com"),
            Affirmation(65735417, R.string.affirmation9, R.drawable.image9, R.string.description9, "444444444", "meDueleLaMuñeca@gmail.com", "meAguanto@gmail.com"),
            Affirmation(96876541, R.string.affirmation10, R.drawable.image10, R.string.description10, "555555555", "yaEstoyAcabando@gmail.com", "yaHeAcabado@gmail.com"))
    }
}
